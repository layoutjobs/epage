$(function () {
    'use strict';

    // Inicializa o widget jQuery File Upload (home):
    $('#fileupload-home').fileupload({
        // Descomente a seguir para enviar cookies cross-domain:
        // xhrFields: {withCredentials: true},
    });

    // Ao completar o upload através do widget jQuery File Upload (home):
    // TODO: validar são não houve erro.

    $('#fileupload-home').bind('fileuploaddone', function(e, data) {
        $('#contato').modal({
         	keyboard: false,
        	backdrop: 'static', 
        });
       	$('#inputSetor').parents('.control-group').hide()
  
    }); 
    
    $('#contato').bind('hide', function(){
    	$('#inputSetor').parents('.control-group').val('2').show()
    })
    
    
    $('a[href="#contato"]').click(function(){
        $('#contato').modal('show');  
    });
 	$('a[href="#preco"]').click(function(){
        $('#preco').modal('show');  
    });

    var offset = $('body').offset();
    var fixedHeight;

    // Obtem a altura dos elementos com posição fixa e alinhado ao topo.
    $('*').each(function() {
        if (/*($(this).css('position') == 'fixed') && ($(this).offset().top <= 0) && */($(this).is(':visible')))
            fixedHeight =+ $(this).outerHeight();  
    });

    // Ao clicar no link "Voltar ao topo".
    $("#voltaTopo").click(function() {
        $('html, body').animate({ scrollTop: 0 }, 500);
    });
	
    // Ao clicar em um item do .navbar, rola a página até a ID do item clicado.
    // Se aplica somente se fixedHeight for diferente de null.
    $('.navbar .nav > li > a').on('click', function(e) {
        if (fixedHeight) {
            var target = $(this).attr('href');
            var targetOffset = $(target).offset();
            var scrollTo = targetOffset.top - fixedHeight;
            e.preventDefault();
            $('html, body').animate({ scrollTop: scrollTo}, 1000);
        }   
    });

    // Inicializa o widget jQuery File Upload:
    $('#fileupload').fileupload({
        // Descomente a seguir para enviar cookies cross-domain:
        // xhrFields: {withCredentials: true},
    });

    // Ativa o acesso iframe cross-domain via opção de redirecionamento:
    $('#fileupload-home').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    // Carrega arquivos existentes:
    $.ajax({
        // Descomente a seguir para enviar cookies cross-domain:
        // xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, null, {result: result});
    });
});

	if($(window).width() >= 1024){
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto();
		});
	}else{
		 $("#myCarousel .item a").on("click",function(){
    		$(window).open($(this).attr("href"))
    	 })
	}
 
    $('#myCarousel').carousel()

  
$('#contato form').submit(function(){
	var erro;
	$(this).find('[required="required"]').each(function(){
		if (!$(this).val()){
			$(this).parents('.control-group').addClass('error')
			erro = "Favor preencher "+$(this).parents('.controls').prev('.control-label').text()
			$(this).parents('.controls').append('<span class="help-inline">'+erro+'</span>')
			
		}	
	})
		//alert ('teste');
	if(erro){
	return false;	
	}
})

$(function(){
	$('#contato').submit(function(){
		var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
		var email = $('#inputEmail').val();
 
		if( email == '' || !er.test(email) ) { alert('Preencha o campo email corretamente'); return false; }
 
	});
});

