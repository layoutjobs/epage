<?php
error_reporting(E_ALL | E_STRICT);

require('UploadHandler.php');

$folder = isset($_POST['folder']) ? $_POST['folder'] : 'files';

// TODO: traduzir mensagens de erro.
$errorMessages = array(
	1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
	2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
	3 => 'The uploaded file was only partially uploaded',
	4 => 'No file was uploaded',
	6 => 'Missing a temporary folder',
	7 => 'Failed to write file to disk',
	8 => 'A PHP extension stopped the file upload',
	'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini',
	'max_file_size' => 'File is too big',
	'min_file_size' => 'File is too small',
	'accept_file_types' => 'Tipo de arquivo não suportado',
	'max_number_of_files' => 'Maximum number of files exceeded',
	'max_width' => 'Image exceeds maximum width',
	'min_width' => 'Image requires a minimum width',
	'max_height' => 'Image exceeds maximum height',
	'min_height' => 'Image requires a minimum height'
);

// TODO: definir parâmetros da classe.
$options = array(
	'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']) . '/temp/' . $folder . '/',
	'upload_url' => getFullUrl() . '/' . $folder . '/',
	'accept_file_types' => '/\.(cdr|pdf|ai|id)$/i', // Tipos de arquivos aceitos.
	'image_versions' => array(), // Não cria versões de images como miniaturas.
);

$uploadHandler = new UploadHandler($options, true, $errorMessages);

function getFullUrl() {
    $https = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off';
    return
        ($https ? 'https://' : 'http://').
        (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
        (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
        ($https && $_SERVER['SERVER_PORT'] === 443 ||
        $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
        substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
}



 