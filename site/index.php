<?php $uploadFolder = uniqid(); //$_SERVER["REMOTE_ADDR"]; ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Epage - Publicação Digital</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Epage é um serviço do estúdio Layout, localizada em Salto-SP. Através do epage, catálogos, manuais ou qualquer material impresso pode ser transformado em digital.">
    <meta name="author" content="Estúdio Layout BR">
    <meta name="keywords" content="Catálogo, Digital, Epage, Manuais, Impresso, Economia, Empresa, Acesso, Agilidade, Custo, Informação"/>
    <meta name="application-name" content="Epage.net.br"/>


    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	 <link href="css/ie7.css" rel="stylesheet"  />

	<!-- Bootstrap CSS fixes for IE6 -->
	<!--[if lt IE 7]><link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->
	<!-- Bootstrap Image Gallery styles -->
	<!--<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">-->
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="css/jquery.fileupload-ui.css">
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>

	<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script src="js/jquery.min.js"></script>
	</head>

  <body>
  


    <!-- NAVBAR
    ================================================== -->
    
   	       
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      	<div class="container">
        	<!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
        	<h1><a class="brand" href="http://www.epage.net.br"><img class="img-menu" src="images/logo.png"  alt="Epage"  /></a></h1>
	            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
             <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
	            <div class="nav-collapse collapse pull-right">
	              <ul class="nav">
	                <li><a href="#o_que_e_epage">O QUE É EPAGE</a></li>
	                <li><a href="#vantagens">VANTAGENS</a></li>
	                <li><a href="#exemplos">EXEMPLOS</a></li>
	                <li><a href="#impresso_x_digital">IMPRESSO x DIGITAL</a></li>
	                <li><a href="arquivos/tabela_preco.pdf" target="_blank">PREÇO</a></li>
	                <li><a href="#contato">CONTATO</a></li>
	                <!-- Read about Bootstrap dropdowns at http://twitter.github.com/bootstrap/javascript.html#dropdowns -->
	               </ul>
	            </div><!--/.nav-collapse -->
        </div>
      </div><!-- /.navbar-inner -->
    </div><!-- /.navbar -->
       
       
	<!--Banner Topo-->
	
	<div class="banner-top">
		<div class="container fundo-banner-top">	
			<div class="container">
			<div class="row">
				<div class="span6">
					<div class="banner-top-epage"><h1><img src="images/banner-top-epage.png" alt="Epage - Economia e Agilidade"></h1></div>
				</div>
				<div class="span6">
					<h3>Transforme seu impresso em digital. Mais economia e agilidade.</h3>
				</div>
			</div>
			</div>
		</div>
	</div>
	<!--Final Banner Topo-->

		
	<!-- O que é epage -->
	<section id="o_que_e_epage" >
		<div class="impressao-digital">
		<div class="container">	
			<div class="row">
				<div class="span6">
					<div class="catalogo-impresso"><h1><img src="images/catalogo-impresso.png" alt="Epage - O que é Epage"></h1></div>
				</div>
				<div class="span6">
					<div class="chamada">
						<ul class="top2">
							<li>Ebook para catálogos, manuais ou livros;</li>	
							<li>Apresentação digital da sua empresa;</li>	
							<li>Acesso em tablets e celulares;</li>	
							<li>Agilidade no acesso e informações;</li>	
							<li>Economia em impressão;</li>	
							<li>Tecnologia de ponta;</li>	
							<li>Ecologicamente correto.</li>
						</ul>
					</div>
					<p class="top"><strong>epage</strong> é um serviço que o estúdio <strong>Layout</strong> oferece para empresas de todos os segmentos. Através do <strong>epage</strong>, catálogos, manuais ou qualquer material impresso pode ser transformado em digital, em formato ebook (livro eletrônico), onde o cliente poderá folhear as páginas, pesquisar através do índice interativo, marcar páginas importantes, imprimir ou ainda fazer download parcial ou completo do catálogo.</p>
				</div>
			</div> 
		</div>
		</div>
    </section>
    <!-- O que é epage -->
    
     <!-- Vantagens -->
     <section id="vantagens">
	     <div class="banner-dispositivo">
	    	 <div class="container"> 
	    		 <div class="row">
					<div class="span3 center">
						<h1><img src="images/dispositivo.png" alt="Epage - Vantagens"></h1>
					</div>	
					<div class="span3">		
						<p class="top3 text-dispositivo">epage pode ser visualizado em qualquer dipositivo móvel com plataforma: IOS, Android ou Windows.</p>
								<div class="clearfix"></div>
					</div>	
					<div class="span6">
						<h2>Agilidade no acesso</h2>
							<p>O catálogo ou manual estará disponível online 24h, e poderá ser acessado de qualquer computador, notebook, tabled, iPhone, iPad ou smartphone.</p>
						
						<h2>Economia e ecologia</h2>
							<p>Com o catálogo ou manual digital disponível online, a empresa poderá produzir menos catálogos impressos, economizando com o custo de gráfica e colaborando com o meio ambiente, diminuindo a utilização de papel e tinta.</p>
						
						<h2>Excelente custo x benefício</h2>
							<p>O epage proporciona diferenciais para a empresa, como agilidade, tecnologia, bom atendimento, inovação, economia e preservação do meio ambiente, além disso agrega um conceito atual e inovador à marca da empresa, aproveitando a tecnologia para criar novas oportunidades de negócios.</p>
					
								<p><strong>Mais detalhes:</strong> consulte especificações técnicas. <a target="_blank" href="arquivos/informacoes_epage.pdf">Clique aqui</a><br />
									ou contate-nos pelo telefone: (11) 4021-4421 </p>
					</div>
				</div> 
			</div>
		</div>  
	</section>
    <!-- Vantagens -->
    
    <!-- Exemplo -->
    <section  id="exemplos">
	    <div class="container"> 
	  		<div class="row">
		  		<div class="span12">
		    		<h4 class="center">Veja os exemplos abaixo e clique para visualizar o epage.</h4>
		    	</div>	
		    	   	<div class="span12">
		    			<div id="myCarousel" class="carousel slide">
				      		<div class="carousel-inner">
				        		<div class="item active">
				          			<a href="http://flippingbook.publ.com/hermanmiller-catalog?iframe=true&width=100%&height=100%" rel="prettyPhoto[iframes]" target="_blank"><img src="images/banner/01.png" /></a>
				                </div>
				                <div class="item">
				          			<a href="http://flippingbook.publ.com/bombardier-presentation?iframe=true&width=100%&height=100%" rel="prettyPhoto[iframes]" target="_blank"><img src="images/banner/02.png" /></a>
				                </div>
				                <div class="item">
				          			<a href="http://flippingbook.publ.com/kraft-foods-brochure?iframe=true&width=100%&height=100%" rel="prettyPhoto[iframes]" target="_blank"><img src="images/banner/03.png" /></a>
				                </div>
				                <div class="item">
				          			<a href="http://flippingbook.publ.com/art-and-design-magazine?iframe=true&width=100%&height=100%" rel="prettyPhoto[iframes]" target="_blank"><img src="images/banner/04.png" /></a>
				                </div>
				                <div class="item">
				          			<a href="http://flippingbook.publ.com/alice?iframe=true&width=100%&height=100%" rel="prettyPhoto[iframes]" target="_blank"><img src="images/banner/05.png" /></a>
				                </div>
				                
				                
				                
				             </div>
				     			 <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				      				<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
				    	</div><!-- /.carousel --> 
				   	</div>
	   			    	
	   		</div>
	   	</div>
   	</section>
    <!-- Exemplo -->
    
    <!-- impresso digital -->
    <section id="impresso_x_digital">
	    <div class="column-laranja top">
		    <div class="column-cinza">
		    	<div class="span6 pull-right	">
		    		<p>O <strong>epage</strong> possuí diversas ferramentas para auxiliar e aprimorar a visualização do seu catálogo / manual, permitindo que o cliente, através do índice interativo, encontre o que procura sem perder tempo. Além disso oferece opções de impressão e download do catálogo em formato PDF. </p>
		    	</div>
		    	<div class="clearfix"></div>
		    </div>
		   
		   	<div class="span6">
		   	 	<h5>Seu cliente terá acesso aos seus produtos, serviços ou informações em tempo real.</h5>
		    </div>
	    </div>
	  		<div class="clearfix"></div>
	   		    <div class="impressao-digital topo">
			   		<div class="container">
			   			<div class="row">
				   			<div class="span6">
				   				<h1>Impresso x Digital</h1>
				   					<p class="top4">Veja abaixo um breve comparativo entre o material impresso e o digital. O material impresso não deve ser substituído, porém a informação digital a cada dia se torna mais eficiente, ágil e com o melhor custo benefício. </p>
				   						<div class="img-center img-impresso"><img class="top4" src="images/impressao-digital.png" alt="Epage - Impresso x Digital"></div>
				   							<div class="clearfix"></div>
				   			</div> 	
				   			<div class="span6 img-center">
				   				<!--<img class="seta" src="images/seta.png">-->
					   				<img class="top4 top800 img-imp" src="images/impresso-digital-epage.png" alt="Epage - Impresso x Digital">
				   				
				   			</div> 	
			   			</div>
			   		</div>
			    </div>
    </section>
    <!-- final impresso digital -->
     <!-- pedido -->  
     <section id="teste-gratuito">
     <div class="pedido">
	   		<div class="container fundo-pedido">
	   			<div class="container">
	   				<div class="row">
		   				<div class="span12">
		   					<h3>Faça um teste sem compromisso!</h3>
		   				</div>
		   				<div class="span6">
		   					<div class="banner-top-epage top4"><img src="images/banner-top-epage.png" alt="Epage - Faça um Teste sem compromisso"></div>
			   			</div> 	
		   			<div class="span6">
		   				<h5 class="top5">Envie seu arquivo</h5>
		   					<p class="white">Mande o arquivo em formato PDF, *CDR, *ID ou *AI.  Vamos criar o epage de algumas páginas , e enviaremos para sua visualização, não havendo custo.</p>
		   						<p>(CDR = CorelDraw / ID = InDesign / AI = Illustrator)</p>
		   						<div class="top4">	
				   					 <form id="fileupload-home" action="uploader.php" method="POST" enctype="multipart/form-data">
				   						 <div class="fileupload-buttonbar">
									      	<!-- The fileinput-button span is used to style the file input field as button -->
							                <span class="btn btn-warning fileinput-button">
							                    <i class="icon-plus icon-white"></i>
							                    <span>Adicionar</span>
							                    <input type="file" name="files[]" multiple>
							                    <input type="hidden" name="folder" value="<?php echo $uploadFolder; ?>" />
							                </span>
							                <button type="submit" class="btn btn-inverse start">
							                    <i class="icon-upload icon-white"></i>
							                    <span>Iniciar Upload</span>
							                </button>
									       </div>
				   								<div class="fileupload-loading"></div>
							        				<br>
								       		 			<!-- The table listing the files available for upload/download -->
								       	 				<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
			   						
				   						</form>
		   							</div>	
		   			</div> 	
	   			</div>
	   		</div>
    	</div>
    </div>
   </section>
    
    <!-- final pedido -->
    <!-- Modal -->
	<div id="contato" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <img src="images/logo.png" alt="Epage - Contato"/>
	  </div>
	  <div class="modal-body">
	   	<form class="form-horizontal" action="recebe-email.php" method="POST">
		   <div class="control-group">
		    	<label class="control-label" for="inputNome">Nome</label>
		    		<div class="controls">
		     		 	<input type="text" name="inputNome" id="inputNome" placeholder="Nome">
		 		   	</div>
		  	</div>
		  	<div class="control-group">
		    	<label class="control-label" for="inputEmpresa">Empresa</label>
		    		<div class="controls">
		     		 	<input type="text" name="inputEmpresa" id="inputEmpresa" placeholder="Empresa">
		 		   	</div>
		  	</div>
		  	<div class="control-group">
		    	<label class="control-label" for="inputEmail">Email</label>
		    	<div class="controls">
		      		<input type="text" required="required" name="inputEmail" id="inputEmail" placeholder="Email"   />
		   	 	</div>
		  	</div>
		   	<div class="control-group">
		    	<label class="control-label" for="inputTelefome">Telefone</label>
			    	<div class="controls">
			      		<input type="text" name="inputTelefone" id="inputTelefone" placeholder="Telefone">
			    	</div>
	 		</div>
	 		<div class="control-group">
		    	<label class="control-label" for="inputSetor">Assunto</label>
			    	<div class="controls">
			      		<select name="inputSetor" id="inputSetor">
			      			<option value="1">Vendas</option>
			      			<option value="2">Dúvidas e Informações</option>
			      			<option value="3">Suporte</option>
			      			<option value="4">Faturamento e Notas</option>
			      		</select>
			    	</div>
	 		</div>
	 		<div class="control-group">
	    		<label class="control-label" for="inputMensagem">Mensagem</label>
	   			 	<div class="controls">
	      				<textarea name="mensagem" rows="3" required="required"></textarea>
	    			</div>
	  		</div>
	  		<input type="hidden" name="folder" value="<?php echo $uploadFolder; ?>" />
	 	
	 	
	
	  </div>
	  	 <div class="modal-footer">
			    <input class="btn btn-warning" type="submit" value="Enviar" />
			    <input class="btn btn-warning" type="reset" value="Limpar" />
			  </div>
	 	</form>
	 
	</div>
	
	<!--<div id="preco" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	       <img src="images/logo.png"/>
	  </div>
	  <div class="modal-body">
	   	
	 
		</div>
	</div>-->
	
      
   
    <div class="container">
        <div class="row bottom">
        <div class="span7">
       		<img class="img-logo" src="images/logo-layout.png" alt="Epage - Layout"/><p>epage é um produto do estúdio Layout Design / Publicidade. <br />Conheça nosso site: <a href="http://www.layoutnet.com.br">www.layoutnet.com.br </a><br />Todos os direitos reservados.</p>
     	</div>
	     	<div class="span5 pull-right ">
	     		 <!-- AddThis Button BEGIN -->
		       	 <div class="addthis_toolbox addthis_default_style pull-right">
		            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		            <a class="addthis_button_tweet"></a>
		            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a> 
		        	</div>
			        <script type="text/javascript" src="js/addthis_widget.js"></script>
	       		 <!-- AddThis Button END -->
	     	</div>
	     	
     	</div>
     	
    </div><!-- /.container -->
    
  
    <div class="clearfix"></div>
	
	<a href="javascript:void(0);" id="voltaTopo" class="fixed"><img src="images/icons-top.png" /></a>

	<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td>{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Iniciar</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td>{% if (!i) { %}
            <button class="btn btn-danger cancel">
                <i class="icon-ban-circle icon-white"></i>
                <span>Cancelar</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td>
            <button class="btn btn-danger delete pull-right" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="icon-trash icon-white"></i>
                <span>Deletar</span>
            </button>
           </td>
    </tr>
{% } %}
</script>

<script src="js/bootstrap.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="js/tmpl.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="js/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="js/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="js/main.js"></script>
<!-- Slideshow-->
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"  />
<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39880353-1', 'epage.net.br');
  ga('send', 'pageview');

</script>

  </body>
</html>
